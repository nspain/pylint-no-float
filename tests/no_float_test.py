import astroid
from pylint.lint import PyLinter

import no_float
import pylint.testutils as testutils


class TestNoFloatChecker(testutils.CheckerTestCase):
    CHECKER_CLASS = no_float.NoFloatChecker

    def test_finds_assignments(self):
        """Test the checker finds float type hints for assignments."""
        assignment_1, assignment_2, assignment_3 = astroid.extract_node(
            """
x: int = 1 #@
y: float = 0.0 #@
z: Decimal = Decimal(0) #@
"""
        )

        self.checker.visit_assign(assignment_1)
        self.checker.visit_assign(assignment_2)
        self.checker.visit_assign(assignment_3)

        self.assertAddsMessages(testutils.Message(msg_id="no-float", node=assignment_2))

    def test_finds_function_args(self):
        """Test the checker finds float type hints for function arguments."""
        func_node = astroid.extract_node(
            """
def test(x: int, y: float, z: Decimal) -> None: #@
    pass
"""
        )
        self.checker.visit_functiondef(func_node)
        self.assertAddsMessages(
            testutils.Message(msg_id="float-func-arg", node=func_node)
        )

    def test_finds_function_return(self):
        """Test the checker finds float type hints for the function return value."""
        func_node = astroid.extract_node(
            """
def test() -> float: #@
    return 0.0
"""
        )
        self.checker.visit_functiondef(func_node)
        self.assertAddsMessages(
            testutils.Message(msg_id="float-func-return", node=func_node)
        )

    def test_finds_method_args(self):
        """Test the checker finds float type hints for method arguments."""
        method_node = astroid.extract_node(
            """
class Test:
    def test(self, x: float) -> Node: #@
        pass
"""
        )
        self.checker.visit_functiondef(method_node)
        self.assertAddsMessages(
            testutils.Message(msg_id="float-func-arg", node=method_node)
        )

    def test_finds_method_return(self):
        """Test the checker finds float type hints for method return value."""
        method_node = astroid.extract_node(
            """
class Test:
    def test(self) -> float: #@
        return 0.0
"""
        )
        self.checker.visit_functiondef(method_node)
        self.assertAddsMessages(
            testutils.Message(msg_id="float-func-return", node=method_node)
        )

    def test_finds_class_properties(self):
        """Test the checker finds float type hints for class properties."""
        prop_1, prop_2, prop_3 = astroid.extract_node(
            """
class Test:
    x: int = 1 #@
    y: float = 0.0 #@
    z: Decimal = Decimal(0) #@
"""
        )

        self.checker.visit_assign(prop_1)
        self.checker.visit_assign(prop_2)
        self.checker.visit_assign(prop_3)
        self.assertAddsMessages(
            testutils.Message(msg_id="float-assign-type", node=prop_2)
        )

    def test_function_def_no_floats(self):
        func_def = astroid.extract_node(
            """
def visit_assign(self, node: astroid.AnnAssign): #@
    if hasattr(node, 'annotation') and node.annotation.name == 'float':
        self.add_message(msgid='no-float', node=node)
        """
        )
        self.checker.visit_functiondef(func_def)
        self.assertNoMessages()

    def test_register(self):
        linter = PyLinter()
        no_float.register(linter)
        assert self.checker.name in linter.get_checker_names()
