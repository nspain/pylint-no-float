import astroid
from pylint.lint import PyLinter

import arg_count
import pylint.testutils as testutils


class TestArgCountChecker(testutils.CheckerTestCase):
    CHECKER_CLASS = arg_count.ArgCountChecker

    def test_too_many_all_req_func(self):
        func_node = astroid.extract_node(
            """
        def test(a, b, c, d, e, f, g, h, i): #@
            pass
        """
        )
        with self.assertAddsMessages(
            testutils.Message(msg_id="too-many-required-params", node=func_node)
        ):
            self.checker.visit_functiondef(func_node)

    def test_too_many_req_func(self):
        func_node = astroid.extract_node(
            """
                def test(a, b, c, d, e, f, g, h, i, j=1, k=2, l=3): #@
                    pass
                """
        )
        with self.assertAddsMessages(
            testutils.Message(msg_id="too-many-required-params", node=func_node)
        ):
            self.checker.visit_functiondef(func_node)

    def test_ok_all_req_func(self):
        func_node = astroid.extract_node(
            """
                def test(a, b, c, d): #@
                    pass
                """
        )
        with self.assertNoMessages():
            self.checker.visit_functiondef(func_node)

    def test_ok_req_func(self):
        func_node = astroid.extract_node(
            """
                        def test(a, b, c, d, e=1, f=2, g=3, h=4, i=5): #@
                            pass
                        """
        )
        with self.assertNoMessages():
            self.checker.visit_functiondef(func_node)

    def test_too_many_all_req_method(self):
        method_node = astroid.extract_node(
            """
            class Test:
               def test(self, a, b, c, d, e, f, g, h, i): #@
                   pass
               """
        )
        with self.assertAddsMessages(
            testutils.Message(msg_id="too-many-required-params", node=method_node)
        ):
            self.checker.visit_functiondef(method_node)

    def test_too_many_req_method(self):
        method_node = astroid.extract_node(
            """
        class Test:
            def test(self, a, b, c, d, e, f, g, h, i, j=1, k=2, l=3): #@
                pass
                        """
        )
        with self.assertAddsMessages(
            testutils.Message(msg_id="too-many-required-params", node=method_node)
        ):
            self.checker.visit_functiondef(method_node)

    def test_ok_all_req_method(self):
        method_node = astroid.extract_node(
            """
                    class Test:
                        def test(self, a, b, c, d, e, f): #@
                            pass
                        """
        )
        with self.assertNoMessages():
            self.checker.visit_functiondef(method_node)

    def test_ok_req_method(self):
        method_node = astroid.extract_node(
            """
                            class Test:
                                def test(self, a, b, c, d, e=1, f=2, g=3, h=4, i=5): #@
                                    pass
                                """
        )
        with self.assertNoMessages():
            self.checker.visit_functiondef(method_node)

    def test_too_many_all_pos_func_args(self):
        call_node = astroid.extract_node(
            """
        test(1, 2, 3, 4, 5, 6, 7, 8) #@
        """
        )
        with self.assertAddsMessages(
            testutils.Message("too-many-pos-args", node=call_node)
        ):
            self.checker.visit_call(call_node)

    def test_too_many_pos_func_args(self):
        call_node = astroid.extract_node(
            """
               test(1, 2, 3, 4, 5, 6, 7, 8, x=2, y=5) #@
               """
        )
        with self.assertAddsMessages(
            testutils.Message("too-many-pos-args", node=call_node)
        ):
            self.checker.visit_call(call_node)

    def test_ok_all_pos_func_args(self):
        call_node = astroid.extract_node(
            """
               test(1, 2, 3) #@
               """
        )
        with self.assertNoMessages():
            self.checker.visit_call(call_node)

    def test_ok_pos_func_args(self):
        call_node = astroid.extract_node(
            """
                       test(1, 2, 3, a=1, b=2, c=3, d=4, e=5, f=6) #@
                       """
        )
        with self.assertNoMessages():
            self.checker.visit_call(call_node)

    def test_too_many_all_pos_method_args(self):
        call_node = astroid.extract_node(
            """
                class Test:
                    def test(self, a, b, c, d, e, f, g, h):
                        pass
                t = Test()
                t.test(1, 2, 3, 4, 5, 6, 7, 8) #@
                """
        )
        with self.assertAddsMessages(
            testutils.Message("too-many-pos-args", node=call_node)
        ):
            self.checker.visit_call(call_node)

    def test_too_many_pos_method_args(self):
        call_node = astroid.extract_node(
            """
                       class Test:
                           def test(self, a, b, c, d, e, f, g, h, i, j, k):
                               pass
                       t = Test()
                       t.test(1, 2, 3, 4, 5, 6, 7, 8, h=9, i=10, j=11, k=12) #@
                       """
        )
        with self.assertAddsMessages(
            testutils.Message("too-many-pos-args", node=call_node)
        ):
            self.checker.visit_call(call_node)

    def test_ok_all_pos_method_args(self):
        call_node = astroid.extract_node(
            """
                        class Test:
                            def test(self, a, b, c):
                                pass
                        t = Test()
                        t.test(1, 2, 3) #@
                        """
        )
        with self.assertNoMessages():
            self.checker.visit_call(call_node)

    def test_ok_pos_method_args(self):
        call_node = astroid.extract_node(
            """
                               class Test:
                                   def test(self, a, b, c, d, e, f, g, h):
                                       pass
                               t = Test()
                               t.test(1, 2, 3, 4, 5, f=6, g=7, h=8) #@
                               """
        )
        with self.assertNoMessages():
            self.checker.visit_call(call_node)

    def test_register(self):
        linter = PyLinter()
        arg_count.register(linter)
        assert self.checker.name in linter.get_checker_names()
