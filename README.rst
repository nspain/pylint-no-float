No float
========

Custom pylint rule to check we're not using floats and if so, recommend the use
of `Decimal`.

Rational
--------

In some situations, Python's `float` type does not provide enough precision or
control over its representation. In this situation, the `decimal.Decimal` type
can be a good alternative.

Install
-------

For general usage, you can just install the checker by running `pip install .` in the
project directory.

Development
~~~~~~~~~~~

For development, it is better to install the checker using `pip install -e .`. This means
that when you make changes to the code, you don't have to reinstall the package.

Distribution
~~~~~~~~~~~~

To build a wheel for distributing the checker, you can use `python setup.py bdist_wheel`.

Usage
-----

To use the checker, you first need to install it. Then it can be used by pylint in two
ways. The first one, when you run `pylint`, specify `pylint_no_float` as an argument
to `--load-plugins` (e.g. `pylint --load-plugins=pylint_no-float` src/`). The second is
to add `pylint_no_float` to the `load-plugins` value in the `.pylintrc` file. The second
option is best if you're using the plugin option.