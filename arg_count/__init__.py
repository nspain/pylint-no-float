import astroid
from astroid.scoped_nodes import FunctionDef
from pylint.checkers import BaseChecker
from pylint.lint import PyLinter

__version__ = "0.0.1"


class ArgCountChecker(BaseChecker):
    """Check the number of function/method arguments.

    This checker is differs from the argument count checker included with pylint in
    that it ignore arguments with default values.
    """

    name = "arg-count"

    msgs = {
        "C0004": (
            "%s has too many required parameters.",
            "too-many-required-params",
            "Too many required parameters.",
        ),
        "C0006": (
            "%s called with too many positional arguments",
            "too-many-pos-args",
            "Too many positional arguments.",
        ),
    }

    options = (
        (
            "max-positional-arguments",
            {
                "default": 7,
                "type": "int",
                "metavar": "<count>",
                "help": "Maximum number of positional arguments",
            },
        ),
        (
            "max-required-parameters",
            {
                "default": 7,
                "type": "int",
                "metavar": "<count>",
                "help": "Maximum number of required parameters",
            },
        ),
    )

    priority = -1

    def visit_functiondef(self, function_def: FunctionDef) -> None:
        """Check if the function/method has too many required arguments."""
        required_count = 0
        for arg in function_def.args.args:
            try:
                function_def.args.default_value(arg.name)
            except astroid.NoDefault:
                required_count += 1

        if required_count > self.config.max_required_parameters:
            self.add_message("too-many-required-params", node=function_def)

    def visit_call(self, call: astroid.Call) -> None:
        """Check if the function/method has too many positional arguments."""
        if len(call.args) > self.config.max_positional_arguments:
            self.add_message("too-many-pos-args", node=call)


def register(linter: PyLinter):
    """Register the checker with pylint."""
    linter.register_checker(ArgCountChecker(linter))
