import setuptools

requirements = ["pylint==2.4.4", "astroid==2.3.3"]
test_requirements = ["pytest", "pytest-cov"]
dev_requirements = ["black", "pycodestyle", "pydocstyle"]

setuptools.setup(
    name="opdms-pylint-checkers",
    version="0.0.1",
    description="Various pylint checkers used by OPDMS.",
    packages=["no_float", "arg_count"],
    install_requires=requirements,
    extra_require={
        "test": test_requirements,
        "dev": requirements + test_requirements + dev_requirements
    }
)
