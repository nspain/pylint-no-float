"""Custom pylint rule to check the float type is not used."""

import astroid

from pylint.interfaces import IAstroidChecker
from pylint.checkers import BaseChecker
from pylint.lint import PyLinter

__version__ = "1.0.0"


class NoFloatChecker(BaseChecker):
    """Check the float type is not being used."""

    __implements__ = IAstroidChecker

    name = "no-float"

    msgs = {
        # Not using floats is a convention so we use the 'C' category.
        "C0001": (
            "'float' type used for assignment type.",
            "float-assign-type",
            "Don't use 'float'. Use 'decimal.Decimal' instead.",
        ),
        "C0002": (
            "'float' type used for function argument.",
            "float-func-arg",
            "Don't use 'float'. Use 'decimal.Decimal' instead",
        ),
        "C0003": (
            "'float' type used for function return.",
            "float-func-return",
            "Don't use 'float'. Use 'decimal.Decimal' instead.",
        ),
    }

    options = ()

    # Checker's priority. Pylint sorts them from most negative to most positive
    priority = -1

    def visit_assign(self, node: astroid.AnnAssign):
        """Check that assignments don't have a 'float' type hint."""
        if hasattr(node, "annotation") and node.annotation.name == "float":
            self.add_message(msgid="float-assign-type", node=node)

    def visit_functiondef(self, node: astroid.FunctionDef):
        """Check the function or method arguments and return type hints aren't floats."""
        for i, annot in enumerate(node.args.annotations):
            if annot is not None and hasattr(annot, "name") and annot.name == "float":
                self.add_message(msgid="float-func-arg", node=node.args.args[i])

        if (
            node.returns
            and hasattr(node.returns, "name")
            and node.returns.name == "float"
        ):
            self.add_message(msgid="float-func-return", node=node)


def register(linter: PyLinter):
    """Register the checker with pylint."""
    linter.register_checker(NoFloatChecker(linter))
